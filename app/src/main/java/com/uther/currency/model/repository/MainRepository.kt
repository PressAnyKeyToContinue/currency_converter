package com.uther.currency.model.repository

import com.uther.currency.entity.Entity
import com.uther.currency.model.data.server.ApiService
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject

class MainRepository
@Inject constructor(private val api: ApiService) {

    fun getRates() : Single<Response<Entity.Rates>> {
        return api.getRates()
            .subscribeOn(Schedulers.newThread())
            .observeOn(Schedulers.computation())
    }

}