package com.uther.currency.model.data.server

import com.uther.currency.entity.Entity
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET


interface ApiService {

    companion object {
        const val BASE_URL: String = "https://api.exchangeratesapi.io/"
    }

    // region ==== exchange rates api io ====

    @GET("latest")
    fun getRates(): Single<Response<Entity.Rates>>

    // endregion

}