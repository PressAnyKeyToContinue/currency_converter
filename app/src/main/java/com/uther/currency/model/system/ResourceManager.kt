package com.uther.currency.model.system

import android.app.Application
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ResourceManager
@Inject constructor(private val context: Application) {
    fun getString(id: Int): String = context.getString(id)
}