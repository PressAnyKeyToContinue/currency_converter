package com.uther.currency.presentation.core

interface FragmentHandler {
    fun fragmentTitle(title: String)
    fun onStartFragment()
    fun onStopFragment()
}