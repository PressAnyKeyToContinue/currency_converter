package com.uther.currency.presentation.core

import android.content.Context
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import dagger.android.support.AndroidSupportInjection
import timber.log.Timber


abstract class BaseFragment : Fragment() {
    @Suppress("PropertyName")
    abstract val LAYOUT_RES: Int
    @Suppress("PropertyName")
    abstract val TAG: String
    @Suppress("PropertyName")
    open var MENU_RES: Int? = null

    private var fragmentHandler: FragmentHandler? = null
    var rootView: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(LAYOUT_RES, container, false)
        return rootView
    }

    override fun onResume() {
        super.onResume()

        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.hideSoftInputFromWindow(rootView!!.windowToken, 0)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (MENU_RES != null)
            setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        if (MENU_RES != null)
            inflater.inflate(MENU_RES!!, menu)
    }


    override fun onCreate(savedInstanceState: Bundle?) {

        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)

        if (activity is FragmentHandler) fragmentHandler = activity as FragmentHandler
        Timber.tag(TAG)
    }

    override fun onStart() {
        super.onStart()
        fragmentHandler?.onStartFragment()
    }

    override fun onStop() {
        super.onStop()
        fragmentHandler?.onStopFragment()
    }

    fun setTitleFragment(title: String) {
        fragmentHandler?.fragmentTitle(title)
    }
}


