package com.uther.currency.presentation.core

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import androidx.transition.TransitionInflater
import com.uther.currency.acc.viewmodel.MyViewModelFactory
import javax.inject.Inject

abstract class BaseViewModelFragment<VM: DisposableViewModel> : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: MyViewModelFactory

    lateinit var vm: VM
    protected abstract fun initViewModel(): VM

    @SuppressLint("ObsoleteSdkInt")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = initViewModel()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        }
    }

    override fun onResume() {
        super.onResume()
        vm = initViewModel()
    }
}