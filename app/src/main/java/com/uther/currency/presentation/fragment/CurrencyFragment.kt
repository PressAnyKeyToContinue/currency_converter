package com.uther.currency.presentation.fragment

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.uther.currency.R
import com.uther.currency.presentation.core.BaseViewModelFragment
import com.uther.currency.util.addOnTextChangedListener
import com.uther.currency.util.addOnItemSelectedListener
import kotlinx.android.synthetic.main.currency_fragment.*
import timber.log.Timber
import javax.inject.Inject


class CurrencyFragment
@Inject constructor(): BaseViewModelFragment<CurrencyViewModel>() {

    override val LAYOUT_RES: Int = R.layout.currency_fragment
    override val TAG: String = javaClass.simpleName

    private var spinnerArrayAdapter: ArrayAdapter<String>? = null

    override fun initViewModel() =
        ViewModelProvider(this, viewModelFactory).get(CurrencyViewModel::class.java)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        llCurrencyConverter?.visibility = View.GONE

        vm.isLoading.observe(viewLifecycleOwner, Observer {
            pbLoadIndicator?.visibility = if (it) View.VISIBLE else View.GONE
        })

        vm.isCached.observe(viewLifecycleOwner, Observer {
            tvCachedData?.visibility = if (it) View.VISIBLE else View.GONE
        })

        vm.errorText.observe(viewLifecycleOwner, Observer {
            Timber.e(it)

            Snackbar
                .make(rootView!!,
                    it?.localizedMessage ?: activity!!.getText(R.string.error_unknown),
                    Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.retry) {
                    vm.updateRates()
                }
                .show()
        })

        vm.getRates().observe(viewLifecycleOwner, Observer {
            val ratesSorted = it.rates.keys.sorted()

            llCurrencyConverter?.visibility = View.VISIBLE

            spinnerArrayAdapter = ArrayAdapter(
                    activity!!, android.R.layout.simple_spinner_dropdown_item, ratesSorted )

            sInputCurrency?.adapter = spinnerArrayAdapter
            sOutputCurrency?.adapter = spinnerArrayAdapter

            sInputCurrency?.setSelection( ratesSorted.indexOf( vm.inputCurrencySelected ) )
            sOutputCurrency?.setSelection( ratesSorted.indexOf( vm.outputCurrencySelected ) )
        })

        etInputCurrency?.addOnTextChangedListener {
            vm.currencyCountChanged(it)
        }

        sInputCurrency?.addOnItemSelectedListener {
            vm.inputCurrencyTypeChanged(it)
        }

        sOutputCurrency?.addOnItemSelectedListener {
            vm.outputCurrencyTypeChanged(it)
        }

        vm.outputCurrencyCount.observe(viewLifecycleOwner, Observer {
            tvOutputCurrency?.text = activity!!.getString(R.string.result_text_format, it)
        })

    }

}
