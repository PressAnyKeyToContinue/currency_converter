package com.uther.currency.presentation.fragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.uther.currency.R
import com.uther.currency.entity.Entity
import com.uther.currency.model.repository.MainRepository
import com.uther.currency.model.system.ResourceManager
import com.uther.currency.presentation.core.DisposableViewModel
import com.uther.currency.util.addTo
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class CurrencyViewModel
@Inject constructor(private val repo: MainRepository,
                    private val resources: ResourceManager): DisposableViewModel() {

    val isLoading = MutableLiveData<Boolean>(false)
    val isCached = MutableLiveData<Boolean>(false)

    var inputCurrencySelected: String? = null
    var outputCurrencySelected: String? = null

    val outputCurrencyCount = MutableLiveData<Double>(0.0)

    private var inputCurrencyRate: Float = 0.0f
    private var outputCurrencyRate: Float = 0.0f
    private var inputCurrencyCount: Double = 0.0

    var errorText = MutableLiveData<Throwable>()

    private val rates: MutableLiveData<Entity.Rates> by lazy {
        MutableLiveData<Entity.Rates>().also {
            updateRates()
        }
    }

    fun getRates(): LiveData<Entity.Rates> {
        return rates
    }

    private fun invokeError(e: Throwable) {
        Timber.e(e)

        errorText.postValue( e )
    }

    fun currencyCountChanged(newValue: String) {
        inputCurrencyCount = newValue.toDoubleOrNull() ?: 0.0

        outputCurrencyCount.postValue( convertCurrency()  )
    }

    fun inputCurrencyTypeChanged(currency: String?) {
        inputCurrencyRate = rates.value?.rates?.get(currency ?: "") ?: 1.0f
        inputCurrencySelected = currency

        outputCurrencyCount.postValue( convertCurrency()  )
    }

    fun outputCurrencyTypeChanged(currency: String?) {
        outputCurrencyRate = rates.value?.rates?.get(currency ?: "") ?: 1.0f
        outputCurrencySelected = currency

        outputCurrencyCount.postValue( convertCurrency()  )
    }

    private fun convertCurrency(): Double {
        return (inputCurrencyCount /  inputCurrencyRate) * outputCurrencyRate
    }

    fun updateRates() {
        repo.getRates()
            .subscribeOn( Schedulers.newThread() )
            .doOnSubscribe { isLoading.postValue(true) }
            .doAfterTerminate { isLoading.postValue(false) }
            .subscribe(
                {
                    val ratesData = it.body()

                    if (ratesData != null) { // cache or data is not null
                        rates.postValue(ratesData)
                        // show notification if no connection
                        isCached.postValue(it.raw().networkResponse == null)
                    }
                    else // cannot connect and cache is empty
                        invokeError(Exception(resources.getString(R.string.error_unable_to_connect)))
                },
                {
                    // another error
                    invokeError(it)
                })
            .addTo(comDisposable)
    }

}
