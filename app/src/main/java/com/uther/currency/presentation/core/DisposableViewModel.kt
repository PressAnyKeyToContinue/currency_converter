package com.uther.currency.presentation.core

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable


/**
 * использовать для моделей с запросами RX,
 * которые необходимо прервать после удаления экрана
 */

abstract class DisposableViewModel : ViewModel() {
    val comDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        comDisposable.dispose()
    }


}
