package com.uther.currency.presentation.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.uther.currency.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
