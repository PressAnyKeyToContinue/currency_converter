package com.uther.currency.presentation.core

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.uther.currency.acc.viewmodel.MyViewModelFactory
import dagger.android.AndroidInjection
import timber.log.Timber
import javax.inject.Inject


abstract class BaseViewModelActivity<VM: DisposableViewModel> : AppCompatActivity() {

    @Suppress("PropertyName") abstract val LAYOUT_RES: Int
    @Suppress("PropertyName") abstract val TAG: String
    @Inject
    lateinit var viewModelFactory: MyViewModelFactory

    private lateinit var vm: VM
    protected abstract fun initViewModel(): VM

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(LAYOUT_RES)
        vm = initViewModel()
        Timber.tag(TAG)
    }

    override fun onResume() {
        super.onResume()
        vm = initViewModel()
    }
}