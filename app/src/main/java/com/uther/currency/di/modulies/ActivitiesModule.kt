package com.uther.currency.di.modulies

import com.uther.currency.presentation.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector(modules = [(FragmentsModule::class)])
    abstract fun contributeMainActivity(): MainActivity

}