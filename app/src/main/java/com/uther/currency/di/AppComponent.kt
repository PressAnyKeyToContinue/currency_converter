package com.uther.currency.di

import android.app.Application
import com.uther.currency.App
import com.uther.currency.di.modulies.ActivitiesModule
import com.uther.currency.di.modulies.AppModule
import com.uther.currency.di.modulies.FragmentsModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [(AndroidInjectionModule::class),
        (AppModule::class),
        (ActivitiesModule::class),
        (FragmentsModule::class)
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)

}