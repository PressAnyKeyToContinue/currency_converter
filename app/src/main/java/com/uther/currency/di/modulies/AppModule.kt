package com.uther.currency.di.modulies

import dagger.Module

@Module(includes = [(ViewModelModule::class), (NetworkModule::class)])
abstract class AppModule