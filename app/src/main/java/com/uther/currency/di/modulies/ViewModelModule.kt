package com.uther.currency.di.modulies

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.uther.currency.acc.viewmodel.MyViewModelFactory
import com.uther.currency.di.ViewModelKey
import com.uther.currency.presentation.fragment.CurrencyViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModelModule {

    // region === Activities ===
    // endregion


    // region === Fragments ===

    @Binds
    @IntoMap
    @ViewModelKey(CurrencyViewModel::class)
    fun bindCurrencyViewModel(viewModel: CurrencyViewModel): ViewModel

    // endregion


    @Binds
    fun bindViewModelFactory(factory: MyViewModelFactory): ViewModelProvider.Factory
}