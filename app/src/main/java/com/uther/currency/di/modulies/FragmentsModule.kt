package com.uther.currency.di.modulies


import com.uther.currency.presentation.fragment.CurrencyFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class FragmentsModule {

    @ContributesAndroidInjector
    abstract fun contributeCurrencyFragment(): CurrencyFragment

}