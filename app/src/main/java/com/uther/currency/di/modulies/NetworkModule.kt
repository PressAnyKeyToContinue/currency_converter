package com.uther.currency.di.modulies

import android.app.Application
import com.uther.currency.model.system.ConnectivityManager
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.uther.currency.BuildConfig
import com.uther.currency.model.data.server.ApiService
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class NetworkModule {
    companion object {
        const val CACHE_SIZE: Long = 1 * 1024 * 1024
        const val CACHE_LIFETIME: Long = 60 * 60 * 24 * 7
        const val REQUEST_TIMEOUT: Long = 30
    }

    @Provides
    fun provideCache(context: Application): Cache {
        return Cache(context.cacheDir, CACHE_SIZE)
    }

    @Provides
    @Singleton
    fun provideHttpClient(cache: Cache, connectivityManager: ConnectivityManager): OkHttpClient {

        val httpClientBuilder = OkHttpClient.Builder()
        httpClientBuilder.readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
        httpClientBuilder.cache(cache)
        httpClientBuilder.addInterceptor(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                var request = chain.request()

                request = if (connectivityManager.isInternetAvailable())
                    // read from network
                    request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build()
                else
                    // read from cache
                    request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=$CACHE_LIFETIME").build()

                return chain.proceed(request)
            }
        })

        // logging
        if (BuildConfig.DEBUG) {
            httpClientBuilder.addNetworkInterceptor(HttpLoggingInterceptor(object :
                HttpLoggingInterceptor.Logger {
                override fun log(message: String) {
                    Timber.tag("OkHttp").d(message)
                }
            }).apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
        }

        return httpClientBuilder.build()
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val builder = GsonBuilder()
        return builder.create()
    }

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
            .baseUrl(ApiService.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    fun provideApi(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)
}