package com.uther.currency.di

import com.uther.currency.App


object AppInjector {
    fun init(app: App) {
        DaggerAppComponent.builder()
                .application(app)
                .build().inject(app)
    }
}