package com.uther.currency.entity

import java.util.*

sealed class Entity {

    data class Rates(
        val rates: Map<String, Float>,
        val base: String,
        val date: Date
    ) : Entity()

}